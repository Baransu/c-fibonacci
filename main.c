#include <stdio.h>

int main (int argc, char* argv[]) {
  int n, a = 1, b = 1;
  sscanf(argv[1], "%d", &n);

  if(n > 1) {
    int wynik, i = 2;
    do {
      wynik = a + b;
      b = a;
      a = wynik;
      i++;
    } while(i < n + 1);
    printf("fib(%d) = %d\n", n + 1, wynik);
  } else {
    printf("n musi byc wieksze od 1\n");
  }

  return 0;
}


